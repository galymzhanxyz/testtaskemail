import imaplib
import email
from email.header import decode_header
import sqlite3
from configparser import ConfigParser
from datetime import datetime

# Чтение конфигурации из файла
config = ConfigParser()
config.read('config.ini')

# Параметры почтового ящика
mail_username = config.get('Mailbox', 'username')
mail_password = config.get('Mailbox', 'password')
mail_server = config.get('Mailbox', 'server')
mail_port = config.getint('Mailbox', 'port')

# Параметры базы данных
db_file = config.get('Database', 'file')

# Список получателей-триггеров
trigger_recipients = config.get('Analyzer', 'trigger_recipients').split(',')

# Подключение к почтовому ящику
mail = imaplib.IMAP4_SSL(mail_server, mail_port)
mail.login(mail_username, mail_password)
mail.select('inbox')

# Подключение к базе данных
conn = sqlite3.connect(db_file)
cursor = conn.cursor()

# Формат сообщения для анализатора (просто для примера)
message_format = config.get('Analyzer', 'message_format')

# Анализ входящих сообщений
status, messages = mail.search(None, 'ALL')
message_ids = messages[0].split()

for message_id in message_ids:
    _, msg_data = mail.fetch(message_id, '(RFC822)')
    msg = email.message_from_bytes(msg_data[0][1])

    sender = msg.get('From')
    subject, _ = decode_header(msg.get('Subject'))[0]
    text_content = msg.get_payload(decode=True).decode('utf-8')

    # Получение времени отправки
    sent_at = datetime.strptime(msg.get('Date'), "%a, %d %b %Y %H:%M:%S %z")

    # Пример условия для проверки формата сообщения
    if message_format in msg.get_payload():
        # Пример сверки данных с данными из базы
        cursor.execute("SELECT * FROM users WHERE subject=? AND sender=?", (subject, sender))
        existing_data = cursor.fetchone()

        if not existing_data:
            # Пример добавления новой строки в базу данных
            cursor.execute("INSERT INTO mails (subject, sender, content, date) VALUES (?, ?, ?, ?, ?)",
                           (subject, sender, text_content, sent_at))
            conn.commit()

# Закрытие соединений
mail.logout()
conn.close()